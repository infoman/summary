//Convertimos la cadena "Hola Rafael" a bytes
var buf = new Buffer('Hola Rafael', 'ascii');
//Mostramos todos los elemento del Buffer
console.log("Elementos del buffer = ");
console.log(buf);
//Mostramos el Buffer en la posición 10
console.log("Buffer en la posición 10 = "+buf[10]);
//Tamaño del Buffer
console.log("Tamaño del buffer = "+buf.length)
//Mostramos elemento por elemento el Buffer
for(var i = 0; i<=buf.length-1; i++)
{
  console.log("Posición = "+i+", elemento = "+buf[i]);
}
